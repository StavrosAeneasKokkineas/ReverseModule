extends Node3D

@onready var characterBody = $"../../"
@onready var cameraR = $"/root/Scene/CameraRotation"

var gravity_vector : Vector2 = ProjectSettings.get_setting("physics/2d/default_gravity_vector")
var gravity_magnitude : int = ProjectSettings.get_setting("physics/2d/default_gravity")

var previousCharacterBodyPosition = Vector3.ZERO;
var previousMousePosition = Vector2.ZERO;
var runSpeed = 450;
# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if (abs(characterBody.velocity.x) < 0.5 && abs(characterBody.velocity.z) < 0.5 && $AnimationPlayer.assigned_animation != "mixamo_com"):
		$AnimationPlayer.play("mixamo_com")
	elif (abs(characterBody.velocity.x) > 0.5 || abs(characterBody.velocity.z) > 0.5):
		$AnimationPlayer.play("Running/mixamo_com")

func _physics_process(delta):
	movementAndCamera(delta)

func movementAndCamera (delta):
	
	characterBody.velocity.y -=  gravity_vector.y * delta
	characterBody.velocity.z = Input.get_axis("ui_up", "ui_down") * cameraR.get_child(0).transform.basis.z.z * runSpeed * delta
	characterBody.velocity.x = Input.get_axis("ui_left", "ui_right") * cameraR.get_child(0).transform.basis.z.z * runSpeed * delta
	
	characterBody.velocity = characterBody.velocity.rotated (Vector3.UP, cameraR.rotation.y)
	characterBody.move_and_slide()
	cameraR.global_position += characterBody.global_position - previousCharacterBodyPosition;
	
	
	var characterRotation = atan2(characterBody.velocity.x, characterBody.velocity.z);
	if ((characterRotation != 0 || Input.is_action_pressed("ui_up"))):
		characterBody.rotation = lerp (characterBody.rotation, Vector3 (characterBody.rotation.x, characterRotation, characterBody.rotation.z), delta * 5) 
	
	cameraR.get_child(0).look_at(self.global_position)
	cameraR.rotation.y += (get_viewport().get_mouse_position().x -  previousMousePosition.x) * delta * 2
	cameraR.rotation.x += (get_viewport().get_mouse_position().y -  previousMousePosition.y) * delta * 2
	cameraR.rotation.x = clamp(cameraR.rotation.x, -PI/8, PI/2)
	
	previousCharacterBodyPosition = characterBody.position
	previousMousePosition = get_viewport().get_mouse_position()
	
